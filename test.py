import unittest
from hw1_solution import score, get_scores, check_word


class ScrabbleCheaterTest(unittest.TestCase):

    def test_score(self):
        assert score('a') == 1
        assert score('A') == 1
        assert score('z') == 10
        assert score('Z') == 10

    def test_get_scores(self):
        assert get_scores(['aa', 'bb'], 'ab') == [('bb', 6), ('aa', 2)]
        assert get_scores(['bb', 'aa'], 'ab') == [('bb', 6), ('aa', 2)]
        assert get_scores(['bb', 'aa', 'ccc'], 'abc') == [('ccc', 9), ('bb', 6), ('aa', 2)]
        assert get_scores(['bb', 'aa', 'ccc'], 'ab') == [('bb', 6), ('aa', 2)]
        assert get_scores(['BB', 'AA', 'CCC'], 'ab') == [('BB', 6), ('AA', 2)]

        assert get_scores(['ba', 'ab'], 'ab') == [('ab', 4), ('ba', 4)]
        assert get_scores(['ba', 'ab'], 'ab') == [('ab', 4), ('ba', 4)]
        assert get_scores(['ac', 'be'], 'abce') == [('ac', 4), ('be', 4)]
        assert get_scores(['aid', 'am'], 'aidm') == [('am', 4), ('aid', 4)]



    def test_check_word(self):
        assert check_word('test', 'est')
        assert not check_word('test', 'esd')
