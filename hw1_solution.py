import argparse
import operator


scores = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2,
          "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3,
          "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1,
          "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4,
          "x": 8, "z": 10}


def score(word):
    result = 0
    for letter in word:
        result += scores[letter.lower()]
    return result


def check_word(word, letters):
    for char in word:
        if not char.lower() in letters.lower():
            return False
    return True


def sort_by(s):
    return (s[1]*-1, len(s[0]), s[0])


def get_scores(words, letters):
    result = dict()
    for word in words:
            word = word.rstrip()
            if check_word(word, letters):
                result[word] = score(word)

    return sorted(result.iteritems(), key=sort_by)


def main(letters, limit):
    with open('sowpods.txt', 'r') as f:
        result = get_scores(f.readlines(), letters)
        if limit == 0:
            limit = len(result)
        print result[:limit]


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("letters")
    parser.add_argument("limit",  nargs='?', type=int, default=0)
    args = parser.parse_args()
    main(args.letters, args.limit)
