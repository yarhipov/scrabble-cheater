# Scrabble Cheater

Write a Python script which determines the highest-scoring words in the board game Scrabble which can be made with a collection of tiles. Word values are simply the sum of individual letter values, with letter values being as follows:

~~~~
scores = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2,
         "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3,
         "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1,
         "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4,
         "x": 8, "z": 10}
~~~~

All legal scrabble words are attached here: sowpods.zip (for evaluation, you may assume the unzipped text file is in the same directory as your script). The script will take the Scrabble "hand" (i.e., tiles) as a sequence of letters, together making the first command-line argument. An optional second command-line argument should specify how many words to output at maximum. In any case, the words should be output one per line, in all caps, followed by a space and then their total score. They should be ordered as follows: first, by score (highest to lowest), then by length (shortest to longest), and finally by alphabetical order.

The following is an example run:
~~~~
$ python hw1_solution.py adfhlex 10
FAXED 16
HEXAD 16
FALX 14
FLAX 14
FLEX 14
FAX 13
HEX 13
AXLED 13
AXED 12
DEX 11
~~~~